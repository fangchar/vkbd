# awesome-vkbd - A virtual keyboard for the awesome window manager

## Limitations
Right now, only two US (us, ansi) and a German (de) keymap are supported.
However, extending with custom keymaps is simple.
Tip: Use xev to find out the necessary keycodes.

Keycodes may differ, depending on the selected X keymap.
Make sure that the selected X keymap matches the virtual keyboard's map.

## Installation

```
cd ~/.config/awesome/
git clone https://codeberg.org/fangchar/awesome-vkbd awesome-vkbd
```

## Usage

```
local vkbd = require("awesome-vkbd")
vkbd.init('de')

-- Now you can add it to a wibar
awful.screen.connect_for_each_screen(function(s)

  s.keyboard = awful.wibar({
    height = 300,
    position = "bottom",
    screen = s
  })

  s.keyboard:setup ({
    layout = wibox.layout.ratio.horizontal,
    vkbd
  })

end)
```

## License
All code is licensed under the terms of the ISC license.
