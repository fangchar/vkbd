-- vkbd - A virtual keyboard for the awesome window manager

local wibox = require("wibox")
local awful = require("awful")

local vkbd = {
  func_pressed  = {},
  homogeneous   = true,
  spacing       = 5,
  min_cols_size = 10,
  min_rows_size = 10,
  expand        = true,
  layout        = wibox.layout.grid,
  font          = 'sans 30',
  smallfont     = 'sans 20',
  key_color_released = '#000000',
  key_color_pressed  = '#555555'
}

local vkbd_widget = wibox.widget(vkbd)

-- Returns a function that handles function keys
function func_key(char)
  -- Helper function for generic function keys
  local function generic()
    if vkbd.func_pressed[char] then
      vkbd.func_pressed[char] = nil
    else
      vkbd.func_pressed[char] = char
    end
  end
  -- Special handling for shift: Toggle shifted keymap
  if(char == 'Shift_L' or char == 'Shift_R') then
    local function shift()
      for _,key in pairs (vkbd_widget:get_all_children()) do
        if (vkbd.func_pressed[char]) then
          key.text = key.unshifted
        else
          key.text = key.shifted
        end
      end
      generic()
    end
    return shift, 'func'
  -- Special handling for altgr: Toggle altgr keymap
  elseif (char == 'ISO_Level3_Shift') then
    local function altgr()
      for _,key in pairs (vkbd_widget:get_all_children()) do
        if (vkbd.func_pressed[char]) then
          key.text = key.unshifted
        else
          key.text = key.altgr
        end
      end
      generic()
    end
    return altgr, 'func'
  -- If no special handling necessary, use the generic function
  else
    return generic, 'func'
  end
end

-- Returns a function that handles caps lock
function caps_lock()
  local function fun ()
    root.fake_input('key_press',   'Caps_Lock')
    root.fake_input('key_release', 'Caps_Lock')
  end
  return fun, 'caps_lock'
end

-- Returns a function that handles regular keys
function easy_key(char)
  local function fun ()
    -- Press all function keys
    for _,func in pairs(vkbd.func_pressed) do
      root.fake_input('key_press', func)
    end

    -- Press and release the actual key
    root.fake_input('key_press',   char)
    root.fake_input('key_release', char)

    -- Release all function keys
    for _,func in pairs(vkbd.func_pressed) do
      root.fake_input('key_release', func)
    end

    -- Unshift all key labels and mark the keys as released
    for _,key in pairs (vkbd_widget:get_all_children()) do
      key.text = key.unshifted
      -- Do not draw toggle keys (e.g. Caps Lock) as released
      if (key.button_type ~= 'caps_lock') then
        key.bg = vkbd.key_color_released
        key.toggle_on = false
      end
    end

    -- Clear all function keys
    vkbd.func_pressed = {}
  end
  return fun, 'regular'
end

-- Callback for changing the key color to pressed
local function paint_key_pressed(widget)
  widget.bg = vkbd.key_color_pressed
end

-- Callback for changing key color to released
local function paint_key_released(widget)
  widget.bg = vkbd.key_color_released
end

-- Callback for toggling key color between pressed and released
local function paint_key_toggle(widget)
  if (widget.toggle_on) then
    widget.bg = vkbd.key_color_released
  else
    widget.bg = vkbd.key_color_pressed
  end
  widget.toggle_on = not widget.toggle_on
end

-- Creates a button with the given text labels and function
local function kbd_key(unshifted, shifted, altgr, fun)
  local result = wibox.widget({
    {
      text = unshifted,
      unshifted = unshifted,
      shifted = shifted,
      altgr = altgr,
      toggle_on = false,
      font = math.max(unshifted:len(),shifted:len(),altgr:len()) < 3 and vkbd.font or vkbd.smallfont,
      align  = 'center',
      valign = 'center',
      ellipsize = 'none',
      widget = wibox.widget.textbox
    },
    bg = vkbd.key_color_released,
    button_type = fun[2],
    widget = wibox.container.background
  })
  result:buttons(awful.util.table.join(awful.button({ }, 1, fun[1])))

  -- Regular keys are marked during press
  -- Other keys are marked when pressed and unmarked when pressed again
  if(result.button_type == 'regular') then
    result:connect_signal('button::press', paint_key_pressed)
    result:connect_signal('button::release', paint_key_released)
  else
    result:connect_signal('button::press', paint_key_toggle)
  end

  return result
end

-- Generates all the keys from a keymap
function vkbd_widget.init(map)
  local keymap = require("awesome-vkbd/keymaps/"..map)
  for _,kbdkey in ipairs(keymap) do
    vkbd_widget:add_widget_at(kbd_key(kbdkey[1], kbdkey[2], kbdkey[3], kbdkey[4]), kbdkey[5], kbdkey[6], kbdkey[7], kbdkey[8])
  end
end

return vkbd_widget
