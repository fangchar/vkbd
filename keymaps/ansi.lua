-- Keymap for ANSI US layout (ansi)

-- Table entry structure:
-- Element 1: Label for unshifted action
-- Element 2: Label for shifted action
-- Element 3: Label for altgr action
-- Element 4: Function to execute on press
-- Element 5: Row
-- Element 6: Column
-- Element 7: Number of rows (optional)
-- Element 8: Number of cols (optional)
local keymap = {
  {'`', '~', '', {easy_key('grave')}, 1, 1 },
  {'1', '!', '', {easy_key('1')}, 1, 2 },
  {'2', '@', '', {easy_key('2')}, 1, 3 },
  {'3', '#', '', {easy_key('3')}, 1, 4 },
  {'4', '$', '', {easy_key('4')}, 1, 5 },
  {'5', '%', '', {easy_key('5')}, 1, 6 },
  {'6', '^', '', {easy_key('6')}, 1, 7 },
  {'7', '&', '', {easy_key('7')}, 1, 8 },
  {'8', '*', '', {easy_key('8')}, 1, 9 },
  {'9', '(', '', {easy_key('9')}, 1, 10},
  {'0', ')}', '', {easy_key('0')}, 1, 11},
  {'-', '_', '', {easy_key('minus')}, 1, 12},
  {'=', '+', '', {easy_key('equal')}, 1, 13},
  {'←', '←', '', {easy_key('BackSpace')}, 1, 14},

  {'⇄', '⇄', '', {easy_key('Tab')}, 2, 1 },
  {'q', 'Q', '', {easy_key('q')}, 2, 2 },
  {'w', 'W', '', {easy_key('w')}, 2, 3 },
  {'e', 'E', '', {easy_key('e')}, 2, 4 },
  {'r', 'R', '', {easy_key('r')}, 2, 5 },
  {'t', 'T', '', {easy_key('t')}, 2, 6 },
  {'y', 'Y', '', {easy_key('y')}, 2, 7 },
  {'u', 'U', '', {easy_key('u')}, 2, 8 },
  {'i', 'I', '', {easy_key('i')}, 2, 9 },
  {'o', 'O', '', {easy_key('o')}, 2, 10},
  {'p', 'P', '', {easy_key('p')}, 2, 11},
  {'[', '{', '', {easy_key('bracketleft')},  2, 12},
  {']', '}', '', {easy_key('bracketright')}, 2, 13},
  {'\\', '|',  '', {easy_key('backslash')}, 2, 14 },
  {'↓', '↓', '', {caps_lock()},   3, 1 },
  {'a', 'A', '', {easy_key('a')}, 3, 2 },
  {'s', 'S', '', {easy_key('s')}, 3, 3 },
  {'d', 'D', '', {easy_key('d')}, 3, 4 },
  {'f', 'F', '', {easy_key('f')}, 3, 5 },
  {'g', 'G', '', {easy_key('g')}, 3, 6 },
  {'h', 'H', '', {easy_key('h')}, 3, 7 },
  {'j', 'J', '', {easy_key('j')}, 3, 8 },
  {'k', 'K', '', {easy_key('k')}, 3, 9 },
  {'l', 'L', '', {easy_key('l')}, 3, 10},
  {';', ':', '', {easy_key('semicolon')}, 3, 11 },
  {'\'', '"', '', {easy_key('apostrophe')}, 3, 12 },
  {'↵', '↵', '', {easy_key('Return')},      3, 13, 1, 2},

  {'↑', '↑', '', {func_key('Shift_L')}, 4, 1, 1, 2},
  {'z', 'Z', '', {easy_key('z')}, 4, 3 },
  {'x', 'X', '', {easy_key('x')}, 4, 4 },
  {'c', 'C', '', {easy_key('c')}, 4, 5 },
  {'v', 'V', '', {easy_key('v')}, 4, 6 },
  {'b', 'B', '', {easy_key('b')}, 4, 7 },
  {'n', 'N', '', {easy_key('n')}, 4, 8 },
  {'m', 'M', '', {easy_key('m')}, 4, 9 },
  {',', '<', '', {easy_key('comma')}, 4, 10 },
  {'.', '>', '', {easy_key('period')}, 4, 11},
  {'/', '?', '', {easy_key('slash')}, 4, 12},
 
  {'Ct\nrl', 'Ct\nrl',  '', {func_key('Control_L')}, 5, 1 },
  {'Mod',  'Mod',   '', {func_key('Super_L')}, 5, 2  },
  {'Alt',  'Alt',   '', {func_key('Alt_L')},   5, 3  },
  {'Space','Space', '', {easy_key('space')},   5, 4, 1, 7 },

  {'Esc', 'Esc', '', {easy_key('Escape')}, 4, 14},

  {'⇧', '⇧', '', {easy_key('Up')},    4, 13},
  {'⇦', '⇦', '', {easy_key('Left')},  5, 12},
  {'⇩', '⇩', '', {easy_key('Down')},  5, 13},
  {'⇨', '⇨', '', {easy_key('Right')}, 5, 14}
}

return keymap
