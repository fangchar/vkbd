-- Keymap for German layout (de)

-- Table entry structure:
-- Element 1: Label for unshifted action
-- Element 2: Label for shifted action
-- Element 3: Label for altgr action
-- Element 4: Function to execute on press
-- Element 5: Row
-- Element 6: Column
-- Element 7: Number of rows (optional)
-- Element 8: Number of cols (optional)
local keymap = {
  {'^', '°', '′', {easy_key('dead_circumflex')}, 1, 1 },
  {'1', '!', '¹', {easy_key('1')}, 1, 2 },
  {'2', '"', '²', {easy_key('2')}, 1, 3 },
  {'3', '§', '³', {easy_key('3')}, 1, 4 },
  {'4', '$', '¼', {easy_key('4')}, 1, 5 },
  {'5', '%', '½', {easy_key('5')}, 1, 6 },
  {'6', '&', '¬', {easy_key('6')}, 1, 7 },
  {'7', '/', '{', {easy_key('7')}, 1, 8 },
  {'8', '(', '[', {easy_key('8')}, 1, 9 },
  {'9', ')}', ']', {easy_key('9')}, 1, 10},
  {'0', '=', '}', {easy_key('0')}, 1, 11},
  {'ß', '?', '\\', {easy_key('ssharp')}, 1, 12},
  {'´', '`', '¸', {easy_key('dead_acute')}, 1, 13},
  {'←', '←', '←', {easy_key('BackSpace')}, 1, 14},

  {'⇒', '⇒', '⇒', {easy_key('Tab')}, 2, 1 },
  {'q', 'Q', '@', {easy_key('q')}, 2, 2 },
  {'w', 'W', 'ſ', {easy_key('w')}, 2, 3 },
  {'e', 'E', '€', {easy_key('e')}, 2, 4 },
  {'r', 'R', '¶', {easy_key('r')}, 2, 5 },
  {'t', 'T', 'ŧ', {easy_key('t')}, 2, 6 },
  {'z', 'Z', '←', {easy_key('z')}, 2, 7 },
  {'u', 'U', '↓', {easy_key('u')}, 2, 8 },
  {'i', 'I', '→', {easy_key('i')}, 2, 9 },
  {'o', 'O', 'ø', {easy_key('o')}, 2, 10},
  {'p', 'P', 'þ', {easy_key('p')}, 2, 11},
  {'ü', 'Ü', '¨', {easy_key('udiaresis')}, 2, 12},
  {'+', '*', '~', {easy_key('plus')},      2, 13},
  {'⏎', '⏎', '⏎', {easy_key('Return')},    2, 14, 2, 1},
  {'↓', '↓', '↓', {caps_lock()}, 3, 1 },
  {'a', 'A', 'æ', {easy_key('a')}, 3, 2 },
  {'s', 'S', 'ſ', {easy_key('s')}, 3, 3 },
  {'d', 'D', 'ð', {easy_key('d')}, 3, 4 },
  {'f', 'F', 'đ', {easy_key('f')}, 3, 5 },
  {'g', 'G', 'ŋ', {easy_key('g')}, 3, 6 },
  {'h', 'H', 'ħ', {easy_key('h')}, 3, 7 },
  {'j', 'J', ' ̣', {easy_key('j')}, 3, 8 },
  {'k', 'K', 'ĸ', {easy_key('k')}, 3, 9 },
  {'l', 'L', 'ł', {easy_key('l')}, 3, 10},
  {'ö', 'Ö', '˝', {easy_key('odiaresis')}, 3, 11 },
  {'ä', 'Ä', '^', {easy_key('adiaresis')}, 3, 12 },
  {'#', '\'', '’', {easy_key('numbersign')}, 3, 13 },

  {'↑', '↑', '↑', {func_key('Shift_L')}, 4, 1 },
  {'<', '>', '|', {easy_key('less')}, 4, 2 },
  {'y', 'Y', '»', {easy_key('y')}, 4, 3 },
  {'x', 'X', '«', {easy_key('x')}, 4, 4 },
  {'c', 'C', '¢', {easy_key('c')}, 4, 5 },
  {'v', 'V', '„', {easy_key('v')}, 4, 6 },
  {'b', 'B', '“', {easy_key('b')}, 4, 7 },
  {'n', 'N', '”', {easy_key('n')}, 4, 8 },
  {'m', 'M', 'µ', {easy_key('m')}, 4, 9 },
  {',', ';', '·', {easy_key('comma')},  4, 10},
  {'.', ':', '…', {easy_key('period')}, 4, 11},
  {'-', '_', '–', {easy_key('minus')},  4, 12},

  {'Ctrl', 'Ctrl',  'Ctrl',  {func_key('Control_L')},  5, 1 },
  {'Mod',  'Mod',   'Mod',   {func_key('Super_L')}, 5, 2  },
  {'Alt',  'Alt',   'Alt',   {func_key('Alt_L')},   5, 3  },
  {'Space','Space', 'Space', {easy_key('space')},   5, 4, 1, 7 },
  {'AltGr','AltGr', 'AltGr', {func_key('ISO_Level3_Shift')}, 5, 11 },

  {'⇧', '⇧', '⇧', {easy_key('Up')},    4, 13},
  {'⇦', '⇦', '⇦', {easy_key('Left')},  5, 12},
  {'⇩', '⇩', '⇩', {easy_key('Down')},  5, 13},
  {'⇨', '⇨', '⇨', {easy_key('Right')}, 5, 14}
}

return keymap
